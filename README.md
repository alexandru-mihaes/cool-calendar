![Logo](assets/images/logo.png)

![Preview](assets/images/preview.png)

# Usage

### 1. The `credentials.json` file must be placed in the source folder (here is `src`), where the Web App will be build. In order to create your credentials, a tutorial is present on the following URL: [Google Calendar API Credentials](https://developers.google.com/calendar/quickstart/js).

*On step **d.** be aware of the Authorized JavaScript origins. For example in the image below, there are three authorized origins that will allow us to build the app locally:*

<img src="./assets/images/authorized-origins.png" width="700" alt="Authorized origins example">

### An example of the content of the `credentials.json` file is presented down below:

<img src="./src/styles/images/scholary-html/credentials.png" width="650" alt="cretentials dot json">

### 2. Use the below script in an HTML and open it in a browser

* **Also the `index.html` file of your web application must be placed in the same directory (`src`) as the `cool-calendar.html` file (the file which is imported in your HTML)**

```html
<html>
  <head>
    <link rel="import" href="{path-to-cool-calendar-web-component}">
  </head>
  <body>
    <cool-calendar calendar-theme="{theme-value}" language="{language-value}"></cool-calendar>
  </body>
</head>
```

### 3. Supported attributes

| **`calendar-theme`** | **`language`** |
| -------------------- |:--------------:|
| `white`              | `english`      |
| `dark`               | `romanian`     |
