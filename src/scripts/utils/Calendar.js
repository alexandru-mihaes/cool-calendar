class Calendar {
    constructor(calendar) {
        this.id = calendar.id;
        this.title = calendar.summary;
        this.backgroundColor = calendar.backgroundColor;
        this.foregroundColor = calendar.foregroundColor;
        this.colorId = calendar.colorId;
    }
}

export default Calendar;