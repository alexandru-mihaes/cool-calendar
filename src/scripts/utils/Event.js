class Event {
    constructor(event, calendarId) {
        this.id = event.id;
        this.calendarId = calendarId;
        this.title = event.summary;
        this.description = event.description;
        if (event.start.date !== undefined) {
            this.startDate = new Date(event.start.date);
        } else {
            this.startDate = new Date(event.start.dateTime);
        }

        if (event.end.date !== undefined) {
            this.endDate = new Date(event.end.date);
        } else {
            this.endDate = new Date(event.end.dateTime);
        }
        this.reminders = event.reminders;
    }
}

export default Event;