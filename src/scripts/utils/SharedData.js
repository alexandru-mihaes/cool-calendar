import Event from "./Event.js";
import Calendar from "./Calendar.js";

let SharedData = (() => {
    let today = new Date();
    let calendarDate = new Date();
    let isSignedIn;
    let calendars = [];
    let events = [];
    let periodRetrieved = [new Date(), new Date()];
    let colors = [];
    let hiddenCalendars = [];

    function getToday() {
        return today;
    }

    function getCalendarDate() {
        return calendarDate;
    }

    function incMonth() {
        calendarDate.setMonth(calendarDate.getMonth() + 1);
    }

    function decMonth() {
        calendarDate.setMonth(calendarDate.getMonth() - 1);
    }

    function incWeek() {
        calendarDate.setDate(calendarDate.getDate() + 7);
    }

    function decWeek() {
        calendarDate.setDate(calendarDate.getDate() - 7);
    }

    function signIn() {
        isSignedIn = true;
        retrieveInitialData();
    }

    function signOut() {
        isSignedIn = false;
    }

    function getLoginStatus() {
        return isSignedIn;
    }

    function setDayToFirst() {
        calendarDate.setDate(1);
    }

    function setToToday() {
        calendarDate.setDate(today.getDate());
    }

    function getEventsByMonth() {
        if (!isSignedIn) {
            return [];
        }
        if (periodRetrieved[0].getTime() <= calendarDate.getTime()) {
            if (periodRetrieved[1].getTime() >= calendarDate.getTime()) {
                var result = [];
                events.forEach((event) => {
                    if (!hiddenCalendars.some((cal) => {
                            return cal.id === event.calendarId
                        })) {
                        if (event.startDate.getMonth() === calendarDate.getMonth()) {
                            result.push(event);
                        }
                        let lastMonth = calendarDate.getMonth() - 1;
                        if (lastMonth == -1) {
                            lastMonth = 11;
                        }
                        if (event.startDate.getMonth() === lastMonth) {
                            result.push(event);
                        }
                        let nextMonth = calendarDate.getMonth() + 1;
                        if (nextMonth == 12) {
                            nextMonth = 0;
                        }
                        if (event.startDate.getMonth() === nextMonth) {
                            result.push(event);
                        }
                    }
                });
                return result;
            } else {
                let right = new Date(calendarDate.getTime());
                right.setMonth(right.getMonth() + 1);
                right.setDate(0);
                retrieveDataForPeriod([new Date(periodRetrieved[1].getTime()), right]);
                periodRetrieved[1] = new Date(right.getTime());
                return 0;
            }
        } else {
            let left = new Date(calendarDate.getTime());
            left.setDate(1);
            retrieveDataForPeriod([left, periodRetrieved[0]]);
            periodRetrieved[0] = new Date(left.getTime());
            return 0;
        }
    }

    function getEvents() {
        if (!isSignedIn) {
            return [];
        }
        let result = [];
        events.forEach((ev) => {
            if (!hiddenCalendars.some((cal) => {
                    cal.id === ev.calendarId
                })) {
                result.push(event);
            }
        });
        return result;
    }

    function getCalendars() {
        return calendars;
    }

    function retrieveDataForPeriod(period) {
        calendars.forEach((cal) => {
            gapi.client.calendar.events.list({
                'calendarId': cal.id,
                'timeMin': period[0].toISOString(),
                'timeMax': period[1].toISOString(),
                'showDeleted': false
            }).then((response) => {
                var eventsRetrieved = response.result.items;
                eventsRetrieved.forEach((ev) => {
                    if (!events.some(function(el) {
                            return el.id == ev.id;
                        })) {
                        events.push(new Event(ev, cal.id));
                    }
                });
                window.postMessage({
                    type: "EVENTS_RECEIVED",
                    payload: null
                }, window.origin);
            });
        });
    }

    function retrieveInitialData() {
        gapi.client.calendar.colors.get().then((response) => {
            colors.calendar = response.result.calendar;
            colors.event = response.result.event;
        });
        var last = new Date(calendarDate.getTime());
        last.setMonth(last.getMonth() - 1);
        last.setDate(1);
        periodRetrieved[0] = new Date(last.getTime());
        var next = new Date(calendarDate.getTime());
        next.setMonth(next.getMonth() + 2);
        next.setDate(0);
        periodRetrieved[1] = new Date(next.getTime());
        gapi.client.calendar.calendarList.list({
            'maxResults': 100
        }).then((response) => {
            var calRetireved = response.result.items;
            calRetireved.forEach((cal) => {
                if (!calendars.some(function(el) {
                        return el.id === cal.id;
                    })) {
                    calendars.push(new Calendar(cal));
                    gapi.client.calendar.events.list({
                        'calendarId': cal.id,
                        'timeMin': last.toISOString(),
                        'timeMax': next.toISOString(),
                        'showDeleted': false,
                        'singleEvents': true,
                        'orderBy': 'startTime'
                    }).then((response) => {
                        var eventsRetrieved = response.result.items;
                        eventsRetrieved.forEach((ev) => {
                            if (!events.some(function(el) {
                                    return el.id == ev.id;
                                })) {
                                events.push(new Event(ev, cal.id));
                            }
                        });
                        window.postMessage({
                            type: "EVENTS_RECEIVED",
                            payload: null
                        }, window.origin);
                    });
                }
            });
            window.postMessage({
                type: "INITIAL_RETRIEVE",
                payload: null
            }, window.origin);
        });
    }

    function getCalendarById(calId) {
        return calendars.find((cal) => {
            return cal.id === calId;
        })
    }

    function getColorById(colorId) {
        return colors.calendar[colorId];
    }

    function hideCalendarById(calId) {
        if (!hiddenCalendars.some((cal) => {
                return cal.id === calId
            })) {
            hiddenCalendars.push(calendars.find((cal) => {
                return cal.id == calId;
            }));
        }
    }

    function showCalendarById(calId) {
        if (hiddenCalendars.some((cal) => {
                return cal.id === calId
            })) {
            let index = hiddenCalendars.findIndex((el) => {
                return el.id == calId;
            });
            hiddenCalendars.splice(index, 1);
        }
    }

    function getDarkerColor(color) {
        let array = [];
        let chars = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f'];
        for (let i = 1; i < color.length; i++) {
            let char = color[i];
            let index = chars.findIndex((el) => {
                return el === char;
            });
            if (index > 1) {
                array.push(chars[index - 2]);
            } else if (index > 0) {
                array.push(chars[index - 1]);
            } else {
                array.push(char);
            }
        }
        let newCollor = "#";
        array.forEach((char) => {
            newCollor += char;
        });
        return newCollor;
    }

    return {
        getToday,
        getCalendarDate,
        incMonth,
        decMonth,
        incWeek,
        decWeek,
        signIn,
        signOut,
        getLoginStatus,
        setDayToFirst,
        setToToday,
        getEventsByMonth,
        getCalendars,
        getEvents,
        getCalendarById,
        getColorById,
        hideCalendarById,
        showCalendarById,
        getDarkerColor
    }
})();

export default SharedData;