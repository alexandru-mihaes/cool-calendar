import WeekView from "../components/WeekView.js";
import MonthView from "../components/MonthView.js";
import CalendarSidebar from "../components/CalendarSidebar.js";
import TopNav from "../components/TopNav.js";
import WebNotification from "./WebNotification.js";

class Main extends HTMLElement {
    constructor() {
        // If you define a constructor, always call super() first as it is required by the CE spec.
        super();
    }

    // Called every time the element is inserted into the DOM
    connectedCallback() {
        this.shadow = this.attachShadow({ mode: 'open' });

        // Select the template and clone it. Finally attach the cloned node to the shadowDOM's root.
        // Current document needs to be defined to get DOM access to imported HTML
        const instance = template.content.cloneNode(true);
        this.shadow.appendChild(instance);
        // In the above snippet we use a template element to clone DOM, instead of setting the innerHTML
        // of the shadowRoot.
        // This technique cuts down on HTML parse costs because the content of the template is only parsed once,
        // whereas calling innerHTML on the shadowRoot will parse the HTML for each instance.

        // Connect to Google Calendar API
        this.handleClientLoad();

        this.defineAttributes();
        this.createComponents();
        this.setTheme();
    }

    // Extract the attribute calendar-theme from our element.
    // Note that we are going to specify our cards like:
    // <cool-calendar calendar-theme="dark"></cool-calendar >
    defineAttributes() {
        this.calendarTheme = this.getAttribute('calendar-theme');
        this.calendarLanguage = this.getAttribute('language');
    }

    createComponents() {
        this.monthView = new MonthView(this.shadow, this.calendarTheme, this.calendarLanguage);
        this.weekView = new WeekView(this.shadow, this.calendarTheme, this.calendarLanguage);
        this.topBar = new TopNav(this.shadow, this.calendarTheme, this.calendarLanguage);
        this.calendarSidebar = new CalendarSidebar(this.shadow, this.calendarTheme);
        WebNotification.getPermission();

        // testing WEB Notification
        setTimeout(() => {
            WebNotification.pushNotification('{event title}', '{hh:mm}', '{hh:mm}');
        }, 4000);

        this.setTheme();
    }

    setTheme() {
        if (this.calendarTheme === 'white') {
            this.shadow.querySelector('.cool-calendar__container').className += ' cool-calendar__container-white';
        } else if (this.calendarTheme === 'dark') {
            this.shadow.querySelector('.cool-calendar__container').className += ' cool-calendar__container-dark';
        }
    }

    handleClientLoad() {
        gapi.load('client:auth2', this.initClient);
    }

    initClient() {
        let updateSigninStatus = function(isSignedIn) {
            if (isSignedIn) {
                window.postMessage({
                    type: "SIGN_IN",
                    payload: null
                }, window.origin);
            } else {
                window.postMessage({
                    type: "SIGN_OUT",
                    payload: null
                }, window.origin);
            }
        };

        let credentials = new XMLHttpRequest();
        credentials.overrideMimeType("application/json");
        credentials.open("GET", "../../credentials.json", true);
        credentials.onreadystatechange = function() {
            if (credentials.readyState === 4 && credentials.status == "200") {
                let data = JSON.parse(credentials.responseText);
                gapi.client.init({
                    apiKey: data.API_KEY,
                    clientId: data.CLIENT_ID,
                    discoveryDocs: data.DISCOVERY_DOCS,
                    scope: data.SCOPE
                }).then(function() {
                        // Listen for sign-in state changes.
                        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                        // Handle the initial sign-in state.
                        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                    },
                    function(error) {
                        console.log(JSON.stringify(error, null, 2));
                    });
            }
        }
        credentials.send(null);
    }
}

customElements.define('cool-calendar', Main);