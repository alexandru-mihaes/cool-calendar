class WebNotification {
    static getPermission() {
        if (Notification.permission !== 'denied') {
            Notification.requestPermission();
        }
    }


    static pushNotification(eventTitle, eventBeginHour, eventEndHour) {
        const description = {
            body: eventTitle + ': ' + eventBeginHour + ' - ' + eventEndHour
        };

        if (Notification.permission === "granted") {
            const title = 'Cool Calendar';
            let notification = new Notification(title, description);
        }
    }
}

export default WebNotification;