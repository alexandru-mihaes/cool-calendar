import SharedData from "../utils/SharedData.js";

class TopNav {
    constructor(shadow, theme, language) {
        this.shadow = shadow;
        this.calendarTheme = theme;
        this.language = language;
        this.init();
    }

    init() {
        this.setTheme();
        this.title = this.shadow.querySelector(".top-nav__title");
        let viewButtons = this.shadow.querySelectorAll(".top-nav__view-btn");
        if (this.language === "romanian") {
            this.monthNames = [
                "Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie",
                "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"
            ];
            viewButtons[0].textContent = "Săptămână";
            viewButtons[1].textContent = "Lună";
        } else {
            this.monthNames = [
                "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            viewButtons[1].textContent = "Week";
            viewButtons[1].textContent = "Month";
        }
        this.authLanguage = this.getAuthLanguage();
        this.title.textContent = this.monthNames[SharedData.getCalendarDate().getMonth()] + " " + SharedData.getCalendarDate().getFullYear();
        this.signInBtn = this.shadow.querySelector(".top-nav__login-btn");
        this.setActionsOnButtons();
        window.addEventListener("message", (event) => {
            this.handleIncomingEvent(event);
        });
        this.currentView = "MONTH";
    }

    getAuthLanguage() {
        if (this.language === "romanian") {
            return ["Conectare", "Deconectare"];
        } else {
            return ["Sign In", "Sign Out"];
        }
    }

    setTheme() {
        if (this.calendarTheme === 'white') {
            this.shadow.querySelector('.top-nav__container').className += ' white-component';
            this.shadow.getElementById('top-nav__download-btn-img').src = 'styles/images/white/download.svg';
            this.shadow.getElementById('top-nav__back-btn-img').src = 'styles/images/white/left-arrow.svg';
            this.shadow.getElementById('top-nav__next-btn-img').src = 'styles/images/white/right-arrow.svg';
        } else if (this.calendarTheme === 'dark') {
            this.shadow.querySelector('.top-nav__container').className += ' dark-component';
            this.shadow.getElementById('top-nav__download-btn-img').src = 'styles/images/dark/download.svg';
            this.shadow.getElementById('top-nav__back-btn-img').src = 'styles/images/dark/left-arrow.svg';
            this.shadow.getElementById('top-nav__next-btn-img').src = 'styles/images/dark/right-arrow.svg';
        }
    }

    setActionsOnButtons() {
        this.setActionsOnViewButtons();
        this.setActionOnNextButton();
        this.setActionOnBackButton();
    }

    setActionsOnViewButtons() {
        // Set actions for change view buttons (Week \ Month)
        const viewButtons = this.shadow.querySelectorAll(".top-nav__view-btn");
        let self = this;
        viewButtons.forEach(btn => {
            btn.addEventListener("click", function() {
                let view = btn.name.toUpperCase();
                if (view === "SĂPTĂMÂNĂ") {
                    view = "WEEK";
                } else if (view === "LUNĂ") {
                    view = "MONTH";
                }
                self.currentView = view;
                window.postMessage({
                    type: "CHANGE_VIEW",
                    payload: {
                        view
                    }
                }, window.origin);
            });
        });
    }

    updateCalendarDate(type) {
        switch (this.currentView) {
            case "WEEK":
                if (type === "INC") {
                    SharedData.incWeek();
                } else {
                    SharedData.decWeek();
                }
                break;
            case "MONTH":
                if (type === "INC") {
                    SharedData.incMonth();
                } else {
                    SharedData.decMonth();
                }
                break;
        }
    }

    setActionOnNextButton() {
        // Set action for Next button
        let self = this;
        let nextBtn = this.shadow.querySelector("#top-nav__next-btn");
        nextBtn.addEventListener("click", function() {
            self.updateCalendarDate("INC");
            window.postMessage({
                type: "MOVE_NEXT",
                payload: null
            }, window.origin);
        });
    }

    setActionOnBackButton() {
        // Set action for Back button
        let self = this;
        let backBtn = this.shadow.querySelector("#top-nav__back-btn");
        backBtn.addEventListener("click", function() {
            self.updateCalendarDate("DEC");
            window.postMessage({
                type: "MOVE_BACK",
                payload: null
            }, window.origin);
        });
    }

    setActionOnSignInButton() {
        switch (SharedData.getLoginStatus()) {
            case true:
                this.shadow.querySelector(".top-nav__login-text").textContent = this.authLanguage[1];
                this.signInBtn.removeEventListener("click", this.signIn);
                this.signInBtn.addEventListener("click", this.signOut);
                break;
            case false:
                this.shadow.querySelector(".top-nav__login-text").textContent = this.authLanguage[0];
                this.signInBtn.removeEventListener("click", this.signOut);
                this.signInBtn.addEventListener("click", this.signIn);
                break;
        }
    }

    signIn() {
        gapi.auth2.getAuthInstance().signIn();
    }

    signOut() {
        gapi.auth2.getAuthInstance().signOut();
    }

    handleIncomingEvent(event) {
        switch (event.data.type) {
            case "MOVE_NEXT":
                this.title.textContent = this.monthNames[SharedData.getCalendarDate().getMonth()] + " " + SharedData.getCalendarDate().getFullYear();
                break;
            case "MOVE_BACK":
                this.title.textContent = this.monthNames[SharedData.getCalendarDate().getMonth()] + " " + SharedData.getCalendarDate().getFullYear();
                break;
            case "SIGN_IN":
                SharedData.signIn();
                this.setActionOnSignInButton();
                break;
            case "SIGN_OUT":
                SharedData.signOut();
                this.setActionOnSignInButton();
                break;
        }
    }
}

export default TopNav;