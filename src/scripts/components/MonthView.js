import SharedData from "../utils/SharedData.js";

class MonthView {
    constructor(shadow, theme, language) {
        this.shadow = shadow;
        this.calendarTheme = theme;
        this.language = language;
        this.init();
    }

    init() {
        this.setTheme();
        this.mainDiv = this.shadow.querySelector(".month-view__container");
        this.createDays();
        this.listOfDays = this.getListOfDays(SharedData.getCalendarDate());
        this.generateDayBoxes();
        window.addEventListener("message", (event) => {
            this.handleIncomingEvent(event);
        });
        this.initialized = false;
        this.eventDivs = [];
    }

    createDays() {
        let days;
        if(this.language === "romanian") {
            days = ["Lun", "Mar", "Mie", "Joi", "Vin", "Sâm", "Dum"];
        } else {
            days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        }
        for(let dayIndex = 0; dayIndex < 7; dayIndex++) {
            let divWeekDay = document.createElement("div");
            divWeekDay.className = "week-day";
            divWeekDay.textContent = days[dayIndex];
            this.mainDiv.appendChild(divWeekDay);
        }
    }

    setTheme() {
        if (this.calendarTheme === 'white') {
            this.shadow.querySelector('.month-view__container').className += ' white-component';
        } else if (this.calendarTheme === 'dark') {
            this.shadow.querySelector('.month-view__container').className += ' dark-component';
        }
    }

    getListOfDays(calendarDate) {
        let listOfDays = [];
        let date = new Date(calendarDate.getTime());
        date.setDate(1);
        let day = date.getDay();
        if (day === 0) {
            day = 7;
        }
        date.setDate(1 - (day - 1));
        let current = false;
        let type = 'no';
        while (listOfDays.length !== 42) {
            if (date.getDate() === 1) {
                if (current === false) {
                    current = true;
                    type = 'yes';
                } else {
                    current = false;
                    type = 'no';
                }
            }
            listOfDays.push([new Date(date.getTime()), type]);
            date.setDate(date.getDate() + 1);
        }
        return listOfDays;
    }

    checkIfDateIsToday(date) {
        let today = SharedData.getToday();
        return (date.getDate() === today.getDate() && date.getMonth() === today.getMonth() && date.getFullYear() === today.getFullYear());
    }

    generateDayBoxes() {
        let count = 0;
        this.monthDivs = [];
        this.listOfDays.forEach(element => {
            count++;
            let newDayBox = document.createElement("div");
            newDayBox.className = 'month-view__day-box';
            if (this.calendarTheme === 'white') {
                newDayBox.className += ' month-view__day-box-white';
            } else if (this.calendarTheme === 'dark') {
                newDayBox.className += ' month-view__day-box-dark';
            }
            if (element[1] === 'no') {
                if (this.calendarTheme === 'white') {
                    newDayBox.className += ' cell-last-or-next-white';
                } else if (this.calendarTheme === 'dark') {
                    newDayBox.className += ' cell-last-or-next-dark';
                }
            }
            if (count > 35) {
                newDayBox.className += ' cell-bottom';
            }
            if (count % 7 === 0) {
                newDayBox.className += ' cell-right';
            }
            var title = document.createElement("div");
            if (this.checkIfDateIsToday(element[0])) {
                title.setAttribute('day-value', "Today");
            } else {
                title.setAttribute('day-value', element[0].getDate());
            }
            title.className = "month-view__day-box__title";
            var id = "" + element[0].getFullYear() + "" + element[0].getMonth() + "" + element[0].getDate();
            newDayBox.setAttribute("id", id);
            newDayBox.appendChild(title);
            let createdBox = this.shadow.querySelector('.month-view__container');
            createdBox.appendChild(newDayBox);
            this.monthDivs.push(newDayBox);
        })
    }

    handleIncomingEvent(event) {
        switch (event.data.type) {
            case "CHANGE_VIEW":
                if (event.data.payload.view === "MONTH") {
                    this.mainDiv.style.display = 'grid';
                    this.updateDayBoxes();
                    this.updateEvents();
                } else {
                    this.mainDiv.style.display = 'none';
                }
                break;
            case "MOVE_NEXT":
                if (this.mainDiv.style.display !== 'none') {
                    this.updateDayBoxes();
                    this.updateEvents();
                }
                break;
            case "MOVE_BACK":
                if (this.mainDiv.style.display !== 'none') {
                    this.updateDayBoxes();
                    this.updateEvents();
                }
                break;
            case "SIGN_IN":
                this.updateEvents();
                break;
            case "SIGN_OUT":
                this.clearEvents();
                break;
            case "INITIAL_RETRIEVE":
                if (!this.initialized) {
                    this.updateEvents();
                    this.initialized = true;
                }
                break;
            case "EVENTS_RECEIVED":
                if (this.mainDiv.style.display !== 'none') {
                    this.updateEvents();
                }
                break;
        }
    }

    updateDayBoxes() {
        this.listOfDays = this.getListOfDays(SharedData.getCalendarDate());
        let index = 0;
        this.monthDivs.forEach((div) => {
            var title = document.createElement("div");
            title.className = "month-view__day-box__title";
            if (this.checkIfDateIsToday(this.listOfDays[index][0])) {
                title.setAttribute('day-value', "Today");
            } else {
                title.setAttribute('day-value', this.listOfDays[index][0].getDate());
            }
            div.removeChild(div.firstChild);
            div.appendChild(title);
            div.className = 'month-view__day-box';
            if (this.calendarTheme === 'white') {
                div.className += ' month-view__day-box-white';
            } else if (this.calendarTheme === 'dark') {
                div.className += ' month-view__day-box-dark';
            }
            if (this.listOfDays[index][1] === 'no') {
                if (this.calendarTheme === 'white') {
                    div.className += ' cell-last-or-next-white';
                } else if (this.calendarTheme === 'dark') {
                    div.className += ' cell-last-or-next-dark';
                }
            }
            if ((index + 1) > 35) {
                div.className += ' cell-bottom';
            }
            if ((index + 1) % 7 === 0) {
                div.className += ' cell-right';
            }
            var id = "" + this.listOfDays[index][0].getFullYear() + "" + this.listOfDays[index][0].getMonth() + "" + this.listOfDays[index][0].getDate();
            div.setAttribute("id", id);
            index++;
        });
    }

    updateEvents() {
        this.clearEvents();
        var events = SharedData.getEventsByMonth();
        if (events) {
            events.forEach((event) => {
                var date = event.startDate;
                var divId = "" + date.getFullYear() + "" + date.getMonth() + "" + date.getDate();
                var parentElement = this.monthDivs.find((el) => {
                    return el.id == divId;
                });
                if (parentElement !== undefined) {
                    var eventDiv = document.createElement("div");
                    eventDiv.className = "month-view__event";
                    var span = document.createElement("span");
                    span.textContent = event.title;
                    eventDiv.appendChild(span);
                    var calendar = SharedData.getCalendarById(event.calendarId);
                    var color = SharedData.getColorById(calendar.colorId);
                    if (calendar.backgroundColor === undefined) {
                        eventDiv.style.backgroundColor = color.background;
                    } else {
                        eventDiv.style.backgroundColor = calendar.backgroundColor;
                    }
                    if (calendar.foregroundColor === undefined) {
                        eventDiv.style.color = color.foreground;
                    } else {
                        eventDiv.style.color = calendar.foregroundColor;
                    }
                    parentElement.appendChild(eventDiv);
                    if (parentElement.childNodes.length > 4) {
                        parentElement.style.gridTemplateRows = "repeat(" + parentElement.childNodes.length + ", minmax(20px, 1fr))";
                        parentElement.style.overflow = "scroll";
                    }
                    this.eventDivs.push(eventDiv);
                }
            });
        }
    }

    clearEvents() {
        this.eventDivs.forEach((event) => {
            try {
                event.parentNode.removeChild(event);
            } catch (err) {}
        });
        this.eventDivs = [];
    }
}

export default MonthView;