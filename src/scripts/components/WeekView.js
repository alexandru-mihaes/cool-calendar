import SharedData from "../utils/SharedData.js";

class WeekView {
    constructor(shadow, theme, language) {
        this.shadow = shadow;
        this.calendarTheme = theme;
        this.language = language;
        console.log(this.language);
        this.init();
    }

    init() {
        this.setTheme();
        this.mainDiv = this.shadow.querySelector(".week-view__container");
        if (this.language === "romanian") {
            this.days = ["a", "Lun", "Mar", "Mie", "Joi", "Vin", "Sâm", "Dum"];
        } else {
            this.days = ["a", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
        }
        this.generateHead();
        this.generateWeekCells();
        // this.generateActivities();
        window.addEventListener("message", (event) => {
            this.handleIncomingEvent(event);
        });
        this.eventDivs = [];
        this.initialized = false;
    }

    setTheme() {
        if (this.calendarTheme === 'white') {
            this.shadow.querySelector('.week-view__container').className += ' white-component';
        } else if (this.calendarTheme === 'dark') {
            this.shadow.querySelector('.week-view__container').className += ' dark-component';
        }
    }

    getDays() {
        var result = []
        var date = new Date(SharedData.getCalendarDate().getTime());
        let day = date.getDay();
        if (day === 0) {
            day = 7;
        }
        date.setDate(date.getDate() - (day - 1));
        for (let i = 0; i < 7; i++) {
            let type = "yes";
            if (date.getMonth() != SharedData.getCalendarDate().getMonth()) {
                type = "no";
            }
            result.push([date.getDate(), type]);
            date.setDate(date.getDate() + 1);
        }
        return result;
    }

    generateHead() {
        let weekHead = this.shadow.querySelector("#week-view__head");
        let div = document.createElement("div");
        weekHead.appendChild(div);

        let index = 0;
        this.weekHeadDivs = []
        this.getDays().forEach((day) => {
            let div = document.createElement("div");
            let h5 = document.createElement("h5");
            h5.textContent = this.days[index + 1];
            let span = document.createElement("span");
            span.textContent = day[0];
            if (day[1] === 'no') {
                if (this.calendarTheme === 'white') {
                    div.className = 'cell-last-or-next-white';
                } else if (this.calendarTheme === 'dark') {
                    div.className = 'cell-last-or-next-dark';
                }
            }
            div.appendChild(h5);
            div.appendChild(span);
            this.weekHeadDivs.push(div);
            weekHead.appendChild(div);
            index += 1;
        });
    }

    updateHead() {
        var days = this.getDays();
        var index = 0;
        this.weekHeadDivs.forEach((div) => {
            div.childNodes[1].textContent = days[index][0];
            if (days[index][1] === 'no') {
                if (this.calendarTheme === 'white') {
                    div.className = 'cell-last-or-next-white';
                } else if (this.calendarTheme === 'dark') {
                    div.className = 'cell-last-or-next-dark';
                }
            } else {
                div.className = "";
            }
            index += 1;
        });
    }

    generateWeekCells() {
        this.weekDivs = [];
        let days = this.getDays();
        for (let index = 0; index < 25; index++) {
            // Generate hour
            var newTimelineCell = document.createElement("div");
            newTimelineCell.className += 'week-view__timeline';
            var hour = '0';
            if (index < 10) {
                hour += index + ':00';
            } else {
                hour = index + ':00';
            }
            let timelineCellText = document.createElement("p");
            timelineCellText.textContent = hour;
            newTimelineCell.appendChild(timelineCellText);
            this.shadow.getElementById('week-view__body').appendChild(newTimelineCell);

            if (index === 24) {
                break;
            }
            // Add activity cells for all days for that hour
            for (let count = 1; count < 8; count++) {
                let newActivityCell = document.createElement("div");
                if (index === 23) {
                    newActivityCell.className += ' cell-bottom';
                }
                if (count % 7 === 0) {
                    newActivityCell.className += ' cell-right';
                }
                let activityCellId = '' + this.days[count] + days[count - 1][0] + index;
                newActivityCell.setAttribute("id", activityCellId);
                this.weekDivs.push(newActivityCell);
                this.shadow.getElementById('week-view__body').appendChild(newActivityCell);
            }
        }
    }

    updateWeekCells() {
        let days = this.getDays();
        for (let index = 0; index < 24; index++) {
            for (let count = 1; count < 8; count++) {
                let activityCellId = '' + this.days[count] + days[count - 1][0] + index;
                this.weekDivs[index * 7 + (count - 1)].setAttribute("id", activityCellId);
            }
        }
    }

    handleIncomingEvent(event) {
        switch (event.data.type) {
            case "CHANGE_VIEW":
                if (event.data.payload.view === "WEEK") {
                    if (this.mainDiv.style.display !== 'grid') {
                        this.mainDiv.style.display = 'grid';
                        if (SharedData.getCalendarDate().getMonth() !== SharedData.getToday().getMonth()) {
                            SharedData.setDayToFirst();
                            this.updateHead();
                            this.updateWeekCells();
                            this.updateEvents();
                        } else {
                            SharedData.setToToday();
                            this.updateHead();
                            this.updateWeekCells();
                            this.updateEvents();
                        }
                    }
                } else {
                    this.mainDiv.style.display = 'none';
                }
                break;
            case "MOVE_NEXT":
                if (this.mainDiv.style.display !== 'none') {
                    this.updateHead();
                    this.updateWeekCells();
                    this.updateEvents();
                }
                break;
            case "MOVE_BACK":
                if (this.mainDiv.style.display !== 'none') {
                    this.updateHead();
                    this.updateWeekCells();
                    this.updateEvents();
                }
                break;
            case "EVENTS_RECEIVED":
                if (this.mainDiv.style.display !== 'none') {
                    this.updateEvents();
                }
                break;
            case "INITIAL_RETRIEVE":
                if (!this.initialized) {
                    this.updateEvents();
                    this.initialized = true;
                }
                break;
        }
    }

    updateEvents() {
        this.clearEvents();
        var events = SharedData.getEventsByMonth();
        if (events) {
            events.filter((ev) => {
                if (ev.endDate.getDate() === ev.startDate.getDate()) {
                    return ev;
                }
            }).forEach((event) => {
                let day = event.startDate.getDay();
                if (day == 0) {
                    day = 7;
                }
                let id = '' + this.days[day] + event.startDate.getDate() + '' + event.startDate.getHours();
                let parentElement = this.weekDivs.find((element) => {
                    return element.id === id;
                });

                if (parentElement !== undefined) {
                    var eventDiv = document.createElement("div");
                    eventDiv.className = "week-view__event";
                    var divEventTitle = document.createElement("div");
                    divEventTitle.textContent = event.title;
                    eventDiv.appendChild(divEventTitle);
                    var calendar = SharedData.getCalendarById(event.calendarId);
                    var color = SharedData.getColorById(calendar.colorId);
                    if (calendar.backgroundColor === undefined) {
                        eventDiv.style.backgroundColor = color.background;
                        eventDiv.style.borderColor = color.background;
                    } else {
                        eventDiv.style.backgroundColor = calendar.backgroundColor;
                        eventDiv.style.borderColor = calendar.backgroundColor;
                    }
                    if (calendar.foregroundColor === undefined) {
                        eventDiv.style.color = color.foreground;
                    } else {
                        eventDiv.style.color = calendar.foregroundColor;
                    }
                    this.setEventDivHeight(eventDiv, event);
                    parentElement.appendChild(eventDiv);
                    this.eventDivs.push(eventDiv);
                }
            });
        }
    }

    setEventDivHeight(eventDiv, event) {
        let eventStartHour = event.startDate.getHours();
        let eventEndHour = event.endDate.getHours();
        let eventStartMinutes = event.startDate.getMinutes();
        let eventEndMinutes = event.endDate.getMinutes();
        let numberOfHeights = eventEndHour - eventStartHour;
        let eventDivHeight = 0;
        for (let indexHeight = 1; indexHeight <= numberOfHeights; indexHeight++) {
            eventDivHeight += 50;
            eventDivHeight += 2;
        }
        let percentageOfMinutes = eventEndMinutes / 60;
        let percentageOfStartMinutes = eventStartMinutes / 60;
        const DEFAULT_HEIGHT_IN_PIXELS = 50;
        let heightOfMinutes = DEFAULT_HEIGHT_IN_PIXELS * percentageOfMinutes;
        let topHeight = DEFAULT_HEIGHT_IN_PIXELS * percentageOfStartMinutes;
        if (eventEndMinutes === 0 && eventStartHour < eventEndHour) {
            eventDivHeight -= 4;
        }
        eventDivHeight += Math.floor(heightOfMinutes);
        let eventTop = Math.floor(topHeight);
        eventDivHeight -= eventTop;
        eventDiv.style.height = '' + eventDivHeight + "px";
        eventDiv.style.top = '' + eventTop + "px";
        if (eventDivHeight < (DEFAULT_HEIGHT_IN_PIXELS / 3)) {
            eventDiv.style.fontSize = "8px";
        } else if (eventDivHeight < (DEFAULT_HEIGHT_IN_PIXELS / 2)) {
            eventDiv.style.fontSize = "10px";
        }
    }

    clearEvents() {
        this.eventDivs.forEach((event) => {
            event.parentNode.removeChild(event);
        });
        this.eventDivs = [];
    }

}

export default WeekView;