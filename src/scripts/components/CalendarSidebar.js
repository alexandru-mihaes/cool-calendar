import SharedData from "../utils/SharedData.js";

class CalendarSidebar {
    constructor(shadow, theme) {
        this.shadow = shadow;
        this.calendarTheme = theme;
        this.init();
    }

    init() {
        this.mainDiv = this.shadow.querySelector(".calendars-sidebar__container__scrolling-container");
        this.setTheme();
        this.setActionsOnScrollButtons();
        this.initialized = false;
        window.addEventListener("message", (event) => {
            this.handleIncomingEvent(event);
        });
    }

    setTheme() {
        if (this.calendarTheme === 'white') {
            this.shadow.querySelector('.calendars-sidebar__container').className += ' white-component';
            this.shadow.querySelector('.calendars-sidebar-scroller').className += ' white-component';
            this.shadow.getElementById('sidebar__up-btn-img').src = 'styles/images/white/up-arrow.svg';
            this.shadow.getElementById('sidebar__down-btn-img').src = 'styles/images/white/down-arrow.svg';
        } else if (this.calendarTheme === 'dark') {
            this.shadow.querySelector('.calendars-sidebar__container').className += ' dark-component';
            this.shadow.querySelector('.calendars-sidebar-scroller').className += ' dark-component';
            this.shadow.getElementById('sidebar__up-btn-img').src = 'styles/images/dark/up-arrow.svg';
            this.shadow.getElementById('sidebar__down-btn-img').src = 'styles/images/dark/down-arrow.svg';
        }
    }

    setActionsOnScrollButtons() {
        let slideUpButton = this.shadow.querySelector('.calendars-sidebar-scroller__slide-up');
        let calendarsContainer = this.shadow.querySelector('.calendars-sidebar__container');
        slideUpButton.addEventListener("click", function() {
            sideScroll(calendarsContainer, 'up', 20, 70, 7);
        });

        let slideDownButton = this.shadow.querySelector('.calendars-sidebar-scroller__slide-down');
        slideDownButton.addEventListener("click", function() {
            sideScroll(calendarsContainer, 'down', 20, 70, 7);
        });

        function sideScroll(element, direction, speed, distance, step) {
            var scrollAmount = 0;
            var slideTimer = setInterval(function() {
                if (direction === 'up') {
                    element.scrollTop -= step;
                } else {
                    element.scrollTop += step;
                }
                scrollAmount += step;
                if (scrollAmount >= distance) {
                    window.clearInterval(slideTimer);
                }
            }, speed);
        }
    }

    generateCalendars() {
        this.calendarDivs = [];
        let calendars = SharedData.getCalendars();
        calendars.sort((a, b) => {
            if (a.title.includes("@")) {
                return -1;
            }
            return 1;
        });
        let first = true;
        calendars.forEach((cal) => {
            let title = cal.title;
            if (first) {
                title = "Personal";
                first = false;
            }
            let calendarDiv = document.createElement("div");
            calendarDiv.className = "calendars-sidebar__field";
            let input = document.createElement("input");
            input.type = "checkbox";
            input.name = "calendar-checkbox";
            input.id = "calendar-sidebar__calendar-" + title;
            input.value = cal.id;
            input.checked = true;
            calendarDiv.appendChild(input);
            let helper = document.createElement("div");
            helper.className = "field__input-helper";
            helper.style.backgroundColor = cal.backgroundColor;
            helper.style.borderColor = SharedData.getDarkerColor(cal.backgroundColor);
            calendarDiv.appendChild(helper);

            let label = document.createElement("label");
            label.setAttribute("for", input.id);
            label.textContent = title;
            calendarDiv.appendChild(label);
            calendarDiv.addEventListener("click", () => {
                if (input.checked === true) {
                    input.checked = false;
                    SharedData.hideCalendarById(input.value);
                    window.postMessage({
                        type: "EVENTS_RECEIVED",
                        payload: null
                    });
                } else {
                    input.checked = true;
                    SharedData.showCalendarById(input.value);
                    window.postMessage({
                        type: "EVENTS_RECEIVED",
                        payload: null
                    });
                }
            });
            this.calendarDivs.push(calendarDiv);
            this.mainDiv.appendChild(calendarDiv);
        });
    }

    handleIncomingEvent(event) {
        switch (event.data.type) {
            case "INITIAL_RETRIEVE":
                if (!this.initialized) {
                    this.initialized = true;
                    this.generateCalendars();
                }
                break;
        }
    }
}

export default CalendarSidebar;